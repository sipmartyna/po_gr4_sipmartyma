import java.util.ArrayList;

public class Pair<T>
{
    public ArrayList<T> swap() {
        T tmp = first;
        first = second;
        second = tmp;

        ArrayList<T>tab = new ArrayList<>();
        tab.add(first);
        tab.add(second);

        return tab;
    }
    public Pair()
        {
            first = null;
            second = null;
        }

    public Pair(T first, T second)
        {
            this.first = first;
            this.second = second;
        }

        public T getFirst()
    {
        return first;
    }

    public T getSecond()
    {
        return second;
    }

    public void setFirst(T newValue)
    {
        first = newValue;
    }
    
    public void setSecond(T newValue)
    {
        second = newValue;
    }

    private T first;
    private T second;
}

