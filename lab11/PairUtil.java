import java.util.ArrayList;

public class PairUtil<T> implements Comparable<T>{

    public int compareTo(T o) {
        return 0;
    }

    public static <T> ArrayList<T> swap(Pair<T>obj){
        ArrayList<T>tab = new ArrayList<>();
        T first = obj.getSecond();
        T second = obj.getFirst();
        tab.add(first);
        tab.add(second);
        return tab;
    }
}
