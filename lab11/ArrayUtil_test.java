import java.time.LocalDate;

public class ArrayUtil_test {
    public static void main(String[] args){
        Integer[]tab={1,2,3,4,5,6,7};
        Integer[]tab2={5,2,3,7,9};
        LocalDate[]tab3={LocalDate.of(1998,3,11), LocalDate.of(1999,4,19), LocalDate.of(2010,1,24)};
        LocalDate[]tab4={LocalDate.of(1999,3,11), LocalDate.of(1997,4,19), LocalDate.of(2012,1,24)};
        System.out.println(ArrayUtil.isSorted(tab));
        System.out.println(ArrayUtil.isSorted(tab2));
        System.out.println(ArrayUtil.isSorted(tab3));
        System.out.println(ArrayUtil.isSorted(tab4));

    }
}
