public class ArrayUtil<T>implements Comparable<T>{

    public int compareTo(T o) {
        return 0;
    }

    public static <T extends Comparable<T>> boolean isSorted(T[]tab){
        for(int i=0; i<tab.length-1; i++){
            if(tab[i].compareTo(tab[i+1])>0){
                return false;
            }
        }
        return true;
    }
    public static <T extends Comparable<T>> int binSearch(T[]tab, T element){
        int poczatek = 0;
        int koniec = tab.length - 1;
        while (poczatek <= koniec) {
            int srodek = (poczatek + koniec) / 2;
            if (element.compareTo(tab[srodek]) > 0) {
                poczatek = srodek + 1;
            }
            else if (element.compareTo(tab[srodek]) < 0){
                poczatek = srodek - 1;
            }
            else{
                return srodek;
            }
        }
        return -1;
    }
}
