import java.util.Scanner;
import java.util.Stack;

public class zad5 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Stack<String> zdanie = new Stack<>();
        String slowo;

        while (true) {
            slowo = scan.nextLine();
            if (zdanie.size() == 0) {
                slowo = slowo.substring(0, 1).toLowerCase() + slowo.substring(1);
                slowo += ".";
            }
            else if (slowo.charAt(slowo.length() - 1) == '.') {
                slowo = slowo.substring(0, slowo.length() - 1);
                slowo = slowo.substring(0, 1).toUpperCase() + slowo.substring(1);
                zdanie.add(slowo);
                break;
            }
            zdanie.add(slowo);
        }

        while (true) {
            if (zdanie.size() == 1) {
                System.out.print(zdanie.pop());
                break;
            }
            System.out.print(zdanie.pop() + " ");
        }
    }
}