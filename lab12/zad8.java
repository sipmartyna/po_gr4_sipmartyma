import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class zad8 {
    public static <T extends Iterable> void print(T tab){
        Iterator obj=tab.iterator();
        while(obj.hasNext()){
            System.out.println(obj.next());
        }
    }
    public static void main(String[] args){
        LinkedList<String>pracownicy=new LinkedList<>();
        String[]imiona={"Paulina", "Zosia", "Bartosz", "Maksymilian", "Nina", "Tomasz", "Wiktor", "Hanna"};
        pracownicy.addAll(List.of(imiona));
        Stack<Integer>liczby=new Stack<>();
        Integer[]liczebnosc={3,7,8,9};
        liczby.addAll(List.of(liczebnosc));
        print(pracownicy);
        print(liczby);
    }
}
