import java.util.LinkedList;
import java.util.List;

public class zad2 {
    public static <T> void redukuj (LinkedList<T>pracownicy, int n){
        int tmp=n-1;
        while(n<=pracownicy.size()){
            pracownicy.remove(n-1);
            n+=tmp;
        }
    }
    public static void main(String[] args) {
        LinkedList<String>pracownicy=new LinkedList<>();
        String[]imiona={"Paulina", "Zosia", "Bartosz", "Maksymilian", "Nina", "Tomasz", "Wiktor", "Hanna"};
        pracownicy.addAll(List.of(imiona));
        System.out.println(pracownicy);
        redukuj(pracownicy, 2);
        System.out.println(pracownicy);
        LinkedList<Integer>liczby=new LinkedList<>();
        Integer[]liczebnosc={7, 9, 13, 27, 11, 32, 44, 56, 78};
        liczby.addAll(List.of(liczebnosc));
        System.out.println(liczby);
        redukuj(liczby, 3);
        System.out.println(liczby);
    }
}
