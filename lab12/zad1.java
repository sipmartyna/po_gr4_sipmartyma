import java.util.LinkedList;
import java.util.List;

public class zad1 {
    public static void redukuj (LinkedList<String>pracownicy, int n){
        int tmp=n-1;
        while(n<=pracownicy.size()){
            pracownicy.remove(n-1);
            n+=tmp;
        }
    }
    public static void main(String[] args){
        LinkedList<String>pracownicy=new LinkedList<>();
        String[]imiona={"Paulina", "Zosia", "Bartosz", "Maksymilian", "Nina", "Tomasz", "Wiktor", "Hanna"};
        pracownicy.addAll(List.of(imiona));
        System.out.println(pracownicy);
        redukuj(pracownicy, 2);
        System.out.println(pracownicy);

    }
}
