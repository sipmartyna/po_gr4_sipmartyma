import java.util.Scanner;

public class zad7 {
    public static void main(String[] args) {
        int n;
        Scanner scan = new Scanner(System.in);
        n = scan.nextInt();
        boolean[]primes=new boolean[n+1];
        for(int i=2; i<=n; i++){
            primes[i]=true;
        }
        for(int i=2; i<Math.sqrt(n); i++){
            if(primes[i]==true){
                for(int k=i*i; k<=n; k+=i){
                    primes[k]=false;
                }
            }
        }
        for(int i=0; i<=n; i++){
            if(primes[i]==true){
                System.out.println(i);
            }
        }
    }
}


