import java.util.LinkedList;
import java.util.List;

public class zad4 {
    public static <T> void odwroc(LinkedList<T>lista){
        LinkedList<T>tmp=new LinkedList<>(lista);
        lista.removeAll(lista);
        for(int i= tmp.size(); i>0; i--){
            lista.add(tmp.get(i-1));
        }
    }
    public static void main(String[] args){
        LinkedList<String>pracownicy=new LinkedList<>();
        String[]imiona={"Paulina", "Zosia", "Bartosz", "Maksymilian", "Nina", "Tomasz", "Wiktor", "Hanna"};
        pracownicy.addAll(List.of(imiona));
        System.out.println(pracownicy);
        odwroc(pracownicy);
        System.out.println(pracownicy);
        LinkedList<Integer>liczby=new LinkedList<>();
        Integer[]liczebnosc={7, 9, 13, 27, 11, 32, 44, 56, 78};
        liczby.addAll(List.of(liczebnosc));
        System.out.println(liczby);
        odwroc(liczby);
        System.out.println(liczby);
    }
}
