public class zad4 {
    public static void main(String arg[]){
        double poczatkowe_saldo = 1000;
        double oprocentowanie = 0.06;
        double saldo_konta_po_pierwszym_roku = 1000 + (1000 * 0.06);
        double saldo_konta_po_drugim_roku = saldo_konta_po_pierwszym_roku + (saldo_konta_po_pierwszym_roku * 0.06);
        double saldo_konta_po_trzecim_roku = saldo_konta_po_drugim_roku + (saldo_konta_po_drugim_roku * 0.06);

        System.out.println("I rok: " + saldo_konta_po_pierwszym_roku);
        System.out.println("II rok: " + saldo_konta_po_drugim_roku);
        System.out.println("III rok: " + saldo_konta_po_trzecim_roku);
    }
}
