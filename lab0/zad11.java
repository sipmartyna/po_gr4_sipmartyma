public class zad11 {
    public static void main(String[] args) {
        System.out.println("Wielkie to szczęście");
        System.out.println("nie wiedzieć dokładnie,");
        System.out.println("na jakim świecie się żyje.");
        System.out.println(" ");
        System.out.println("Trzeba by było");
        System.out.println("istnieć bardzo długo,");
        System.out.println("stanowczo dłużej");
        System.out.println("niż istnieje on");
        System.out.println(" ");
        System.out.println("Choćby dla porównania");
        System.out.println("poznać inne światy.");
        System.out.println(" ");
        System.out.println("unieść się ponad ciało");
        System.out.println("które niczego tak dobrze nie umie,");
        System.out.println("jak ograniczać");
        System.out.println("i stwarzać trudności.");
        System.out.println(" ");
        System.out.println("Dla dobra badań,");
        System.out.println("jasności obrazu");
        System.out.println("i ostatecznych wniosków");
        System.out.println("wzbić się ponad czas,");
        System.out.println("w którym to wszystko pędzi i wiruje.");
        System.out.println(" ");
        System.out.println("Z tej perspektywy");
        System.out.println("żegnajcie na zawsze");
        System.out.println("szczegóły i epizody.");
        System.out.println(" ");
        System.out.println("Liczenie dni tygodnia");
        System.out.println("musiałoby się wydawać");
        System.out.println("czynnością bez sensu,");
        System.out.println("wrzucenie listu do skrzynki");
        System.out.println("wybrykiem głupiej młodości,");
        System.out.println(" ");
        System.out.println("napis “Nie deptać trawy”");
        System.out.println("napisem szalonym.");
    }

}
