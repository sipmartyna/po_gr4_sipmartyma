package zadanie_z_wykladu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class WordslnHashSetDemo {
    public static void main(String[] args) {
        // Klasa HashSet implementuje interfejs Set
        try {
            Set<String> words = new HashSet<>(); //5908 unikalnych słów. 2-8 milisekund.
//            Set<String> words = new TreeSet<>(); //5908 unikalnych słów. 16-19 milisekund.
            long totalTime = 0;

            File file = new File("src/alice30.txt");

            Scanner in = new Scanner(file);
            while (in.hasNext()) {
                String word = in.next();
                long callTime = System.currentTimeMillis();
                words.add(word);
                callTime = System.currentTimeMillis() - callTime;
                totalTime += callTime;
            }

            Iterator<String> iter = words.iterator();
            for (int j = 1; j <= 20; ++j) {
                System.out.println(iter.next());
            }

            System.out.println(". . .");
            System.out.println(words.size() + " unikalnych słów. " + totalTime + " milisekund.");
        } catch (FileNotFoundException aE) {
            aE.printStackTrace();
        }
    }
}
