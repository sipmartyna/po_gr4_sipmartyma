package pl.imiajd.sip;

public class Nauczyciel extends Osoba {
    private double pensja;

    public Nauczyciel(String nazwisko, int rok_urodzenia, double pensja) {
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }

    public String toString() {
        return super.toString() + "pensja" + pensja;
    }

    public double getPensja() {
        return pensja;
    }
}
