package pl.imiajd.sip;

import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{
    BetterRectangle(int width, int height){
        super(width, height);
    }
    public double getPerimeter(){
        double perimeter = (2 * width) + (2 * height);
        return perimeter;
    }
    public double getArea(){
        double area = width * height;
        return area;
    }
}
