package pl.imiajd.sip;

public class Osoba_test {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("Zemlik", 2000);
        System.out.println(osoba1);
        System.out.println(osoba1.getNazwisko());
        System.out.println(osoba1.getRok_urodzenia());

        Student student1 = new Student("Tomasiak", 1999, "biotechnologia");
        System.out.println(student1);
        System.out.println(student1.getNazwisko());
        System.out.println(student1.getRok_urodzenia());
        System.out.println(student1.getKierunek());

        Nauczyciel nauczyciel1 = new Nauczyciel("Ebertowski", 1979, 5200);
        System.out.println(nauczyciel1);
        System.out.println(nauczyciel1.getNazwisko());
        System.out.println(nauczyciel1.getRok_urodzenia());
        System.out.println(nauczyciel1.getPensja());
    }
}