package pl.imiajd.sip;

public class Adres {
    private String  ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz(){
        System.out.println(kod_pocztowy+" "+miasto);
        if(numer_mieszkania==0) {
            System.out.println(ulica + " " + numer_domu);
        }
        else {
            System.out.println(ulica + " " + numer_domu + " " + numer_mieszkania);
        }
    }

    public boolean przed(Adres adres1){
        char pierwsza_cyfra_1 = kod_pocztowy.charAt(0);
        char pierwsza_cyfra_2 = adres1.kod_pocztowy.charAt(0);
        char druga_cyfra_1 = kod_pocztowy.charAt(1);
        char druga_cyfra_2 = adres1.kod_pocztowy.charAt(1);

        String kod_1_str = String.valueOf(pierwsza_cyfra_1) + String.valueOf(druga_cyfra_1);
        String kod_2_str = String.valueOf(pierwsza_cyfra_2) + String.valueOf(druga_cyfra_2);
        if(pierwsza_cyfra_1 == 0){
            kod_1_str = String.valueOf(druga_cyfra_1);
        }
        if(pierwsza_cyfra_2 == 0){
            kod_2_str = String.valueOf(druga_cyfra_2);
        }
        int kod_1_int = Integer.parseInt(kod_1_str);
        int kod_2_int = Integer.parseInt(kod_2_str);

        if(kod_1_int > kod_2_int){
            return true;
        }
        else{
            return false;
        }
    }


}

