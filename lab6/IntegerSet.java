import java.util.ArrayList;

public class IntegerSet{
    boolean[] tab;
    IntegerSet(){
        tab=new boolean[100];
    }

    public static IntegerSet union(IntegerSet tab1, IntegerSet tab2){
        IntegerSet tab3=new IntegerSet();
        for(int i=0; i<100; i++){
            if(tab1.tab[i] == true || tab2.tab[i] == true){
                tab3.tab[i]=true;
            }
        }
        return tab3;
    }

    public static IntegerSet intersection(IntegerSet tab1, IntegerSet tab2){
        IntegerSet tab3=new IntegerSet();
        for(int i=0; i<100; i++){
            if(tab1.tab[i] == true && tab2.tab[i] == true){
                tab3.tab[i]=true;
            }
        }
        return tab3;
    }

    public void insertElement(int n){
        tab[n-1]=true;
    }

    public void deleteElement(int n){
        tab[n-1]=false;
    }

    public String toString(){
        StringBuffer napis=new StringBuffer();
        for(int i=0; i<100; i++){
            if(tab[i] == true){
                napis.append(i+1);
                napis.append(" ");
            }
        }
        return napis.toString();
    }

    public boolean equals(IntegerSet tab2){
        ArrayList<Boolean> tab3=new ArrayList<Boolean>();
        int ile_wspolnych=0;
        int ile_true=0;
        for(int i=0; i<100; i++){
            if(tab[i] == true && tab2.tab[i] == true){
                tab3.add(true);
                ile_wspolnych+=1;
            }
        }
        for(int i=0; i<100; i++){
            if(tab[i] == true){
                ile_true+=1;
            }
        }
        if(ile_true == ile_wspolnych){
            return true;
        }
        return false;
    }
}


