public class IntegerSet_test {
    public static void main(String[] args){
        IntegerSet tab1=new IntegerSet();
        IntegerSet tab2=new IntegerSet();
        tab1.insertElement(1);
        tab1.insertElement(2);
        tab1.insertElement(3);
        System.out.println(tab1);

        tab2.insertElement(3);
        tab2.insertElement(4);
        tab2.insertElement(5);
        System.out.println(tab2);

        IntegerSet tab3=IntegerSet.union(tab1, tab2);
        System.out.println(tab3);

        IntegerSet tab4=IntegerSet.intersection(tab1, tab2);
        System.out.println(tab4);

        if(tab1.equals(tab2) == true){
            System.out.println("tab1 i tab2 sa takie same");
        }
        else{
            System.out.println("tab1 i tab2 nie sa takie same");
        }
    }
}
