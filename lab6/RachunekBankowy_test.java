public class RachunekBankowy_test {
    public static void main(String[] args){
        RachunekBankowy saver1=new RachunekBankowy(2000.00);
        RachunekBankowy saver2=new RachunekBankowy(3000.00);
        RachunekBankowy.setRocznaStopaProcentowa(4);
        System.out.println("Oszczednosci przy rocznej stopie procentowej rownej 4%:");
        System.out.println(saver1.obliczMiesieczneOdsetki());
        System.out.println(saver2.obliczMiesieczneOdsetki());
        RachunekBankowy.setRocznaStopaProcentowa(5);
        System.out.println("Oszczednosci przy rocznej stopie procentowej rownej 5%:");
        System.out.println(saver1.obliczMiesieczneOdsetki());
        System.out.println(saver2.obliczMiesieczneOdsetki());
    }
}
