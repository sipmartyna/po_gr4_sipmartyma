public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;
    RachunekBankowy(double saldo){
        this.saldo=saldo;
    }
    public double obliczMiesieczneOdsetki(){
        double odsetki;
        odsetki=(saldo*rocznaStopaProcentowa)/12;
        saldo+=odsetki;
        saldo = Math.round(this.saldo * 100.0) / 100.0;
        return saldo;
    }
    public static void setRocznaStopaProcentowa(double x){
        double procent=x/100;
        rocznaStopaProcentowa=procent;
    }
}
