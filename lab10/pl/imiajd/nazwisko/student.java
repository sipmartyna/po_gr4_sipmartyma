package pl.imiajd.nazwisko;

import java.time.LocalDate;

public class student extends osoba implements Comparable<osoba>{
    double sredniaOcen;

    public student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String toString(){
        return super.toString()+sredniaOcen;
    }

    public int compareTo(student obj){
        int wynik=super.compareTo(obj);
        if(wynik == 0){
            if(this.sredniaOcen > obj.sredniaOcen){
                return 1;
            }
            else if(this.sredniaOcen < obj.sredniaOcen){
                return -1;
            }
        }
        return wynik;
    }


}
