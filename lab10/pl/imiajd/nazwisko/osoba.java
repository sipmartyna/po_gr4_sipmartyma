package pl.imiajd.nazwisko;

import java.time.LocalDate;

public class osoba implements Cloneable, Comparable<osoba> {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String toString() {
        return this.getClass().getSimpleName()+"["+nazwisko+" "+dataUrodzenia+"]";
    }

    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        osoba that = (osoba) obj;
        if(!this.nazwisko.equals(that.nazwisko)){
            return false;
        }
        if(!this.dataUrodzenia.equals(that.dataUrodzenia)){
            return false;
        }
        return true;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int compareTo(osoba obj) {
        int wynik = this.nazwisko.compareTo(obj.nazwisko);
        if (wynik == 0) {
            wynik = this.dataUrodzenia.compareTo(obj.dataUrodzenia);
            return wynik;
        }
        return wynik;
    }




}
