package pl.imiajd.nazwisko;

import pl.imiajd.nazwisko.osoba;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class osoba_test {
    public static void main(String[] args){
        ArrayList<osoba>grupa = new ArrayList<>();
        grupa.add(new osoba("szymczak", LocalDate.of(1997,3,17)));
        grupa.add(new osoba("szymczak", LocalDate.of(1988,5,12)));
        grupa.add(new osoba("sip", LocalDate.of(2000,1,24)));
        grupa.add(new osoba("kowalczuk", LocalDate.of(2005,9,11)));
        grupa.add(new osoba("impol", LocalDate.of(2005,9,11)));
        for(int i=0; i< grupa.size(); i++){
            System.out.println(grupa.get(i));
        }
        System.out.println(" ");
        Collections.sort(grupa);
        for(int i=0; i< grupa.size(); i++){
            System.out.println(grupa.get(i));
        }
    }
}
