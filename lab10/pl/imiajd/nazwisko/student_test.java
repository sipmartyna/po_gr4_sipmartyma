package pl.imiajd.nazwisko;

import pl.imiajd.nazwisko.student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class student_test {
    public static void main(String[] args){
        ArrayList<student>grupa = new ArrayList<>();
        grupa.add(new student("szymczak", LocalDate.of(1997,3,17),3.5 ));
        grupa.add(new student("szymczak", LocalDate.of(1988,5,12),4.1));
        grupa.add(new student("sip", LocalDate.of(2000,1,24), 4.7));
        grupa.add(new student("kowalczuk", LocalDate.of(2005,9,11),3.0));
        grupa.add(new student("impol", LocalDate.of(2005,9,11),2.7));

        for(int i=0; i< grupa.size(); i++){
            System.out.println(grupa.get(i));
        }
        System.out.println(" ");
        Collections.sort(grupa);
        for(int i=0; i< grupa.size(); i++){
            System.out.println(grupa.get(i));
        }

    }
}
