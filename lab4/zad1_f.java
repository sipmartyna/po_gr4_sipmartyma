import java.util.Scanner;

public class zad1_f {
    public static String change(String str){
        StringBuffer buffer = new StringBuffer();
        for(int i = 0; i < str.length(); i++){
            char znak1 = str.charAt(i);
            if(Character.isLowerCase(znak1)){
                char znak2 = Character.toUpperCase(znak1);
                buffer.append(znak2);
            }
            else{
                char znak2 = Character.toLowerCase(znak1);
                buffer.append(znak2);
            }
        }

        String slowo = buffer.toString();
        return slowo;
    }

    public static void main(String[] args){
        Scanner skaner = new Scanner(System.in);
        System.out.print("Podaj slowo: ");
        String slowo = skaner.nextLine();
        System.out.println("Zmienione slowo: " + change(slowo));
    }
}
