import java.util.Scanner;

public class zad1_h {
    public static String nice(String str, char znak){
        StringBuffer buffer = new StringBuffer();
        int indeks = 0;
        for(int i = str.length() - 1; i >= 0; i--){
            if(indeks % 3 == 0 && indeks != 0){
                buffer.append(znak);
            }
            buffer.append(str.charAt(i));
            indeks += 1;
        }
        buffer.reverse();
        String liczba = buffer.toString();
        return liczba;
    }

    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        String liczba = skaner.nextLine();
        System.out.print("Podaj znak: ");
        char znak = skaner.nextLine().charAt(0);
        System.out.println("Zmieniona liczba: " + nice(liczba, znak));
    }
}
