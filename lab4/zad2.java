import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad2 {
    public static int ilosc_znakow(String str, char znak){
        int ilosc = 0;
        for(int i = 0; i < str.length(); i++){
            char znak1 = str.charAt(i);
            if(znak1 == znak){
                ilosc += 1;
            }
        }
        return ilosc;
    }

    public static void main(String[] args) throws FileNotFoundException {
        File plik = new File("C:\\Users\\sipma\\IdeaProjects\\lab4\\plik.txt");
        Scanner skaner = new Scanner(plik);
        String plik1= "";
        while(skaner.hasNextLine()){
            plik1 = plik1.concat(skaner.nextLine() + "\n");
        }
        System.out.println("Ilosc wystpaien znaku: " + ilosc_znakow(plik1, 'e'));
    }
}
