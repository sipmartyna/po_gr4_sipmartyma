import java.math.BigInteger;
import java.util.Scanner;

public class zad4 {
    public static BigInteger suma(BigInteger n){
        BigInteger n_petla = new BigInteger("0");
        BigInteger suma = new BigInteger("0");
        BigInteger jedynka = new BigInteger("1");
        BigInteger dwojka = new BigInteger("2");
        BigInteger ziarnko = new BigInteger("1");
        BigInteger n_potega = n.multiply(n);

        while(n_petla.compareTo(n_potega) != 1){
            suma = suma.add(ziarnko);
            ziarnko = ziarnko.multiply(dwojka);
            n_petla = n_petla.add(jedynka);
        }
        return suma;
    }
    public static void main(String[] args){
        System.out.print("Podaj wartosc n: ");
        Scanner skaner = new Scanner(System.in);
        BigInteger n = new BigInteger(skaner.nextLine());
        System.out.println(suma(n));
    }
}
