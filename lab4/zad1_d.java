import java.util.Scanner;

public class zad1_d {
    public static String repeat(String str, int ilosc){
        String slowo = "";
        for(int i = 0; i < ilosc; i++){
            slowo += str;
        }
        return slowo;
    }

    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.print("Podaj slowo: ");
        String slowo = skaner.nextLine();
        System.out.print("Podaj ilosc powtorzen: ");
        int ilosc = skaner.nextInt();
        System.out.println("Slowo " + slowo + " powtarza sie " + ilosc + " razy: " + repeat(slowo, ilosc));
    }
}
