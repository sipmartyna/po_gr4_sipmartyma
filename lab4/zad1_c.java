import java.util.Scanner;

public class zad1_c {
    public static String middle(String str){
        String litery = "";
        int str_len = str.length();
        if(str_len % 2 == 0){
            char znak1 = str.charAt(str_len/2 - 1);
            char znak2 = str.charAt(str_len/2);
            litery += znak1;
            litery += znak2;
        }
        else{
            char znak3 = str.charAt(str_len / 2);
            litery += znak3;
        }
        return litery;
    }

    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.print("Podaj slowo: ");
        String slowo = skaner.nextLine();
        System.out.println("Srodkowa litera/y : " + middle(slowo));
    }
}
