import java.util.Scanner;

public class zad1_a {

    public static int countChar(String str, char znak){
        int ilosc = 0;
        for(int i = 0; i < str.length(); i++){
            char znak1 = str.charAt(i);
            if(znak1 == znak){
                ilosc += 1;
            }
        }
        return ilosc;
    }

    public static void main(String[] args){
        Scanner skaner = new Scanner(System.in);
        System.out.print("Slowo: ");
        String slowo = skaner.nextLine();
        System.out.print("Ilosc c w slwoie: " + countChar(slowo, 'c'));
    }
}
