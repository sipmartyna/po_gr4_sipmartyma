import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Scanner;

public class zad5 {
    public static BigDecimal odsetki(BigDecimal k, BigDecimal p, BigDecimal n){
        BigDecimal n_petla = new BigDecimal("0");
        BigDecimal jedynka = new BigDecimal("1");
        while(n_petla.compareTo(n) != 1){
            k = k.add(k.multiply(p));
            n_petla = n_petla.add(jedynka);
        }
       BigDecimal wynik = k.setScale(2, RoundingMode.CEILING);
        return wynik;
    }


    public static void main(String[] args){
        Scanner skaner = new Scanner(System.in);
        System.out.print("Stan poczatkowy: ");
        BigDecimal k = new BigDecimal(skaner.nextLine());
        System.out.print("Stopa procentowa: ");
        BigDecimal p = new BigDecimal(skaner.nextLine());
        System.out.print("Ilsoc lat: ");
        BigDecimal n = new BigDecimal(skaner.nextLine());
        System.out.println("Kapital jest rowny: " + odsetki(k, p, n));
    }


}
