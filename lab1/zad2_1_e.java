import java.util.Scanner;

public class zad2_1_e {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int rezultat = 0;
        double[] tab = new double[n];
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            double wynik = scan.nextDouble();
            tab[i] = wynik;
        }
        for (int i = 0; i < n; i++) {
            int silnia = 1;
            if (i == 0 || i == 1) {
                silnia = 1;
            } else {
                for (int j = 2; j <= i; j++) {
                    silnia *= j;
                }
            }
                if (tab[i] > Math.pow(2, i) && tab[i] < silnia) {
                    rezultat += 1;
                }

        }
        System.out.print(rezultat);
    }
}
