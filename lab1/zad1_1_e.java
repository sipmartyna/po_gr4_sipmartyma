import java.util.Scanner;

public class zad1_1_e {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int iloczyn = 1;
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            iloczyn *= Math.abs(wynik);
        }
        System.out.println("Iloczyn to: " + iloczyn);
    }
}

