import java.util.Scanner;

public class zad2_1_h {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int rezultat = 0;
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            tab[i] = wynik;
        }
        for(int i = 0; i < n; i++){
            if(Math.abs(tab[i]) < Math.pow(i,2)){
                rezultat += 1;
            }
        }
        System.out.print(rezultat);
    }
}
