import java.util.Scanner;

public class zad2_4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int maxi = Integer.MIN_VALUE;
        int mini = Integer.MAX_VALUE;
        int[]tab = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            tab[i] = wynik;
            if(tab[i] < mini){
                mini = tab[i];
            }
            if(tab[i] > maxi){
                maxi = tab[i];
            }
        }
        System.out.println("Najwieksza liczba to: " + maxi);
        System.out.println("Najmniejsza liczba to: " + mini);

    }
}
