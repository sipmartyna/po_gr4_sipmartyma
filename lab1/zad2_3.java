import java.util.Scanner;

public class zad2_3{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int rezultat1 = 0;
        int rezultat2 = 0;
        int rezultat3 = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            if(wynik > 0){
                rezultat1 += 1;
            }
            if(wynik == 0){
                rezultat2 += 1;
            }
            if(wynik < 0){
                rezultat3 += 1;
            }

        }
        System.out.println("Ilosc liczb dodatnich to: " + rezultat1);
        System.out.println("Ilosc liczb rownych zero to: " + rezultat2);
        System.out.print("Ilosc liczb ujemnych to: " + rezultat3);

    }
}

