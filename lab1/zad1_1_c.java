import java.util.Scanner;

public class zad1_1_c {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int suma = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            suma += Math.abs(wynik);
        }
        System.out.println("Suma to: " + suma);
    }
}
