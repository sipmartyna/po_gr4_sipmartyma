import java.util.Scanner;

public class zad1_1_b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int iloczyn = 1;
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            iloczyn *= wynik;
        }
        System.out.println("Iloczyn n liczb naturalnych to: " + iloczyn);
    }
}
