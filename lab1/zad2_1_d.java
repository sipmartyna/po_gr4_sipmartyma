import java.util.Scanner;

public class zad2_1_d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int rezultat = 0;
        double[] tab = new double[n];
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            double wynik = scan.nextDouble();
            tab[i] = wynik;
        }
        for(int i = 1; i < n-1; i++){
            if(tab[i] < (tab[i-1] + tab[i+1])/2){
                rezultat += 1;
            }
        }
        System.out.print(rezultat);
    }
}
