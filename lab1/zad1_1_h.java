import java.util.Scanner;

public class zad1_1_h {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int n = scan.nextInt();
        int rezultat = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartosc: ");
            int wynik = scan.nextInt();
            rezultat = (int) Math.pow(-1,i+1)*wynik;
        }
        System.out.println("Wynik działania to: " + rezultat);
    }
}
