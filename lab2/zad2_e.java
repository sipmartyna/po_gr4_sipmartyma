import java.util.Random;
import java.util.Scanner;

public class zad2_e {
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]) {
        int aktualna_dlugosc = 0;
        int max_dlugosc = 0;

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] >= 0) {
                aktualna_dlugosc += 1;
                if (aktualna_dlugosc > max_dlugosc) {
                    max_dlugosc = aktualna_dlugosc;
                }
            } else {
                aktualna_dlugosc = 0;
            }
        }
        return max_dlugosc;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[] tab = new int[n];
        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(1998) - 999;
            System.out.println(tab[i]);
        }
        System.out.print(dlugoscMaksymalnegoCiaguDodatnich(tab));
    }
}
