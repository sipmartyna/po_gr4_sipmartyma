import java.util.Random;
import java.util.Scanner;

public class zad2_g {
    public static void odwrocFragment(int tab[], int lewy, int prawy) {
        int tmp = 0;
        for(int i = 0; i < (prawy/lewy); i++) {
            tmp = tab[lewy + i];
            tab[lewy + i] = tab[prawy - i];
            tab[prawy - i] = tmp;
        }
        for(int j = 0; j < tab.length; j++) {
            System.out.print(tab[j] + ", ");
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj liczbe w zakresie 1 <= n <= 100: ");
        int n = scan.nextInt();

        int[] tab = new int[n];

        Random generator = new Random();
        for(int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(1998)-999;
        }

        System.out.print("Podaj wartosc lewej granicy: ");
        int lewa = scan.nextInt();
        System.out.print("Podaj wartosc prawej granicy: ");
        int prawa = scan.nextInt();
        int[] tab_zamiana = tab.clone();
        odwrocFragment(tab_zamiana, lewa, prawa);

    }
}
