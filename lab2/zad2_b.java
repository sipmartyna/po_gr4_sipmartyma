import java.util.Random;
import java.util.Scanner;

public class zad2_b {
    public static int ileDodatnich(int tab[]) {
        int dodatnie = 0;
        for(int i = 0; i < tab.length; i++){

            if(tab[i] > 0){
                dodatnie += 1;
            }

        }
        return dodatnie;
    }
    public static int ileUjemnych(int tab[]) {
        int ujemne = 0;
        for (int i = 0; i < tab.length; i++) {

            if (tab[i] < 0) {
                ujemne += 1;
            }
        }
        return ujemne;
    }

    public static int ileZerowych(int tab[]){
        int zerowe = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] == 0){
                zerowe += 1;
            }
        }
        return zerowe;
    }

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[] tab = new int[n];
        Random generator = new Random();

        for(int i = 0; i < n; i++){
            tab[i] = generator.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        System.out.print(ileDodatnich(tab));
        System.out.print(ileUjemnych(tab));
        System.out.print(ileZerowych(tab));
    }
}


