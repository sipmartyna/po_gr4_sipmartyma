import java.util.Random;
import java.util.Scanner;

public class zad1_b {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[]tab = new int[n];
        int ujemne = 0;
        int dodatnie = 0;
        int zerowe = 0;
        Random generator = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = generator.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        for(int i = 0; i < n; i++){
            if(tab[i] < 0){
                ujemne += 1;
            }
            if(tab[i] > 0){
                dodatnie += 1;
            }
            if(tab[i] == 0){
                zerowe += 1;
            }
        }
        System.out.println("Ilosc liczb dodatnich to: " + dodatnie);
        System.out.println("Ilosc liczb ujemnych to: " + ujemne);
        System.out.println("Ilosc liczb rownych zero to: " + zerowe);


    }
}
