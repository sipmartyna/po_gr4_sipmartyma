import java.util.Random;
import java.util.Scanner;

public class zad2_d {
    public static int sumaDodatnich (int tab[]){
        int suma_dodatnich = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] >= 0){
                suma_dodatnich += tab[i];
            }
        }
        return suma_dodatnich;
    }
    public static int sumaUjemnych (int tab[]){
        int suma_ujemnych = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] < 0){
                suma_ujemnych += tab[i];
            }
        }
        return suma_ujemnych;

    }
    public static void main(String[] args){
            Scanner scan = new Scanner(System.in);
            System.out.print("Podaj liczbe calkowita od 1 do 100: ");
            int n = scan.nextInt();
            int[] tab = new int[n];
            Random generator = new Random();

            for(int i = 0; i < n; i++){
                tab[i] = generator.nextInt(1998)-999;
                System.out.println(tab[i]);
            }
            System.out.println(sumaDodatnich(tab));
            System.out.print(sumaUjemnych(tab));
        }
    }

