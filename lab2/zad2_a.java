import java.util.Random;
import java.util.Scanner;

public class zad2_a {
    public static int ileNieparzystych (int tab[]){
        int nieparzyste = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i]%2 != 0){
                nieparzyste += 1;
            }
        }
        return nieparzyste;

    }
    public static int ileParzystych (int tab[]){
        int parzyste = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i]%2 == 0){
                parzyste += 1;
            }
        }
        return parzyste;

    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[] tab = new int[n];
        Random generator = new Random();

        for(int i = 0; i < n; i++){
            tab[i] = generator.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        System.out.print(ileParzystych(tab));
        System.out.print(ileNieparzystych(tab));
    }
}
