import java.util.Random;
import java.util.Scanner;

public class zad2_c {
    public static int ileMaksymalnych(int tab[]) {
        int maxi = Integer.MIN_VALUE;
        int ilosc = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > maxi) {
                maxi = tab[i];
            }
        }
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == maxi) {
                ilosc += 1;
            }
        }
        return ilosc;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[] tab = new int[n];

        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        System.out.print(ileMaksymalnych(tab));

    }
}
