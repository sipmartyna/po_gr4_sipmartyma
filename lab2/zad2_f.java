import java.util.Random;
import java.util.Scanner;

public class zad2_f {
    public static void signum(int[] tab){
        for(int i = 0; i < tab.length; i++){
            if(tab[i] >= 0){
                tab[i] = 1;
            }
            else{
                tab[i] = -1;
            }
            System.out.print(tab[i]);
        }
    }

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[] tab = new int[n];
        Random generator = new Random();

        for(int i = 0; i < n; i++){
            tab[i] = generator.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        signum(tab);

    }

}
