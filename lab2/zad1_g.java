import java.util.Random;
import java.util.Scanner;

public class zad1_g {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        System.out.print("Podaj wartosc lewej granicy: ");
        int lewa = scan.nextInt();
        System.out.print("Podaj wartosc prawej granicy: ");
        int prawa = scan.nextInt();

        int[] tab = new int[n];
        int tmp = 0;

        Random generator = new Random();
        for(int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(1998)-999;
        }

        for(int i = 0; i < (lewa/prawa); i++){
            tmp = tab[lewa + i];
            tab[lewa + i] = tab[prawa - i];
            tab[prawa - i] = tmp;
        }
        for(int j = 0; j < tab.length; j++) {
            System.out.print(tab[j] + ", ");
        }
    }
}
