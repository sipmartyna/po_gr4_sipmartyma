import java.util.Random;
import java.util.Scanner;

public class zad1_d {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe calkowita od 1 do 100: ");
        int n = scan.nextInt();
        int[]tab = new int[n];
        int suma_ujemnych_elementow = 0;
        int suma_dodatnich_elementow = 0;
        Random generator = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = generator.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        for(int i = 0; i < n; i++){
            if(tab[i] < 0){
                suma_ujemnych_elementow += tab[i];
            }
            if(tab[i] >= 0){
                suma_dodatnich_elementow += tab[i];
            }
        }
        System.out.println("Suma dodatnich elementow to: " + suma_dodatnich_elementow);
        System.out.println("Suma ujemnych elementow to: " + suma_ujemnych_elementow);

    }
}
