package pl.imiajd.sip;

import pl.imiajd.sip.Osoba;
import pl.imiajd.sip.Pracownik;
import pl.imiajd.sip.Student;

import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba(new String[]{"Aleksandra", "Kinga"}, "Zemlik", 2000,
                true, LocalDate.of(2000, 9, 19));

        System.out.println(osoba1);
        System.out.println(osoba1.getNazwisko());
        System.out.println(osoba1.getRok_urodzenia());
        System.out.println(osoba1.getImiona());
        System.out.println(osoba1.getDataUrodzenia());
        System.out.println(osoba1.getPlec());

        Pracownik pracownik1 = new Pracownik(new  String[]{"Damian", "Aleksander"},
                "Kluczek", 1978, false, LocalDate.of(1978, 5, 16),
                7010, LocalDate.of(2007, 9, 3));

        System.out.println(pracownik1);
        System.out.println(pracownik1.getNazwisko());
        System.out.println(pracownik1.getRok_urodzenia());
        System.out.println(pracownik1.getPensja());
        System.out.println(pracownik1.getDataUrodzenia());
        System.out.println(pracownik1.getDataZatrudnienia());
        System.out.println(pracownik1.getImiona());
        System.out.println(pracownik1.getPlec());

        Student student1 = new Student(new String[]{"Krystian"}, "Bobel", 1998,
                false, LocalDate.of(1998, 2,22), "weterynaria", 4.2);

        System.out.println(student1);
        System.out.println(student1.getNazwisko());
        System.out.println(student1.getRok_urodzenia());
        System.out.println(student1.getDataUrodzenia());
        System.out.println(student1.getImiona());
        System.out.println(student1.getPlec());
        System.out.println(student1.getKierunek());
        System.out.println(student1.getSredniaOcen());
        student1.setSredniaOcen(3.8);
        System.out.println(student1.getSredniaOcen());
    }
}
