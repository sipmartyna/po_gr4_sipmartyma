package pl.imiajd.sip;

import pl.imiajd.sip.Osoba;

import java.time.LocalDate;

public class Student extends Osoba {
    private String kierunek;
    private double sredniaOcen;

    public Student(String[] imiona, String nazwisko, int rok_urodzenia, boolean plec,
                   LocalDate dataUrodzenia, String kierunek, double sredniaOcen) {
        super(imiona, nazwisko, rok_urodzenia, plec, dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String toString() {
        return super.toString() + "kierunek " + kierunek + "srednia ocen" + sredniaOcen;
    }

    public String getKierunek() {
        return kierunek;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }
}
