package pl.imiajd.sip;

import java.time.LocalDate;

public abstract class Instrument {
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public abstract void dzwiek();

    public String toString(){
        return "rok produkcji " + rokProdukcji + " producent " + producent;
    }

    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        Instrument that = (Instrument) obj;
        if(!this.producent.equals(that.producent)){
            return false;
        }
        if(!this.rokProdukcji.equals(that.rokProdukcji)){
            return false;
        }
        return true;
    }


}