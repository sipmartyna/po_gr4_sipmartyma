package pl.imiajd.sip;

import java.time.LocalDate;

public class Osoba {
    private String[] imiona;
    private String nazwisko;
    private int rok_urodzenia;
    private boolean plec;
    private LocalDate dataUrodzenia;


    public Osoba(String[] imiona, String nazwisko, int rok_urodzenia, boolean plec, LocalDate dataUrodzenia) {
        this.imiona = imiona;
        this.nazwisko = nazwisko;
        this.rok_urodzenia = rok_urodzenia;
        this.plec = plec;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String toString() {
        StringBuffer lista = new StringBuffer();
        for(int i=0; i<imiona.length; i++){
            lista.append(imiona[i]);
            lista.append(" ");
        }
        return "nazwisko " + nazwisko + " rok urodzenia " + rok_urodzenia +
                " plec " + plec + " data urodzenia " + dataUrodzenia + " imiona " + lista.toString();
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRok_urodzenia() {
        return rok_urodzenia;
    }

    public String getImiona() {
        StringBuffer lista = new StringBuffer();
        for(int i=0; i<imiona.length; i++) {
            lista.append(imiona[i]);
            lista.append(" ");
        }
        return lista.toString();
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public boolean getPlec() {
        return plec;
    }
}

