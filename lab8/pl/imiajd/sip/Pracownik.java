package pl.imiajd.sip;

import pl.imiajd.sip.Osoba;

import java.time.LocalDate;

public class Pracownik extends Osoba {
        private double pensja;
        private LocalDate dataZatrudnienia;

        public Pracownik(String[] imiona, String nazwisko, int rok_urodzenia,
                         boolean plec, LocalDate dataUrodzenia, double pensja, LocalDate dataZatrudnienia) {
                super(imiona, nazwisko, rok_urodzenia, plec, dataUrodzenia);
                this.pensja = pensja;
                this.dataZatrudnienia = dataZatrudnienia;
        }

        public String toString() {
                return super.toString() + "pensja" + pensja + "data zatrudnienia" + dataZatrudnienia;
        }

        public double getPensja() {
                return pensja;
        }

        public LocalDate getDataZatrudnienia() {
                return dataZatrudnienia;
        }
}
