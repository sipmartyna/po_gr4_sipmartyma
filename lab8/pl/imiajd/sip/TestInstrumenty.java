package pl.imiajd.sip;

import pl.imiajd.sip.Flet;
import pl.imiajd.sip.Fortepian;
import pl.imiajd.sip.Instrument;
import pl.imiajd.sip.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args){
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Fortepian("Fazioli", LocalDate.of(1897, 8, 14)));
        orkiestra.add(new Flet("Nippon", LocalDate.of(1894, 10,16)));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2002, 1, 24)));
        orkiestra.add(new Fortepian("Kawai", LocalDate.of(2017, 12, 30)));
        orkiestra.add(new Flet("Gakki", LocalDate.of(1996, 2, 15)));

        for(int i=0; i<orkiestra.size(); i++){
            orkiestra.get(i).dzwiek();
        }
        for(int i=0; i<orkiestra.size(); i++){
            System.out.println(orkiestra.get(i));
        }
    }
}
